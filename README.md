# Configuration and deployment

This example of microservices infra will contain:

* etcd node for discovery and configuration bootstrap
* zuul api gateway
* keycloak for single sign on
* ELK for logging
* 4 REST services:
 * insurance-products-micro - catalog of available products
 * customers-micro - registry of all customers
 * insurance-policy-micro - sold insurance policies
 * point-of-sale-micro - kind of orchestrator service for all of the above

You can find [deployment diagram](https://www.draw.io/?chrome=0&lightbox=1&edit=https%3A%2F%2Fwww.draw.io%2F%23Daxa-micro-deployment.html&nav=1#Daxa-micro-deployment.html) here.

## Services

In order to build services go to `services` directory and run `mvn install`.

## Docker  

We are going to use docker to setup all components. In order to ease with
setup and installation we will provide single point of entry, a docker compose
descriptor, in `deploy\docker-compose.yml`

Once you build all services, run `docker-compose build` and than `docker-compose up` from `deploy` directory.

## Single sign on configuration

First you will need to lunch keycloak container:

```
docker run --name keycloak -p 8080:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin jboss/keycloak
```

In order to make all things work you need additional steps to configure Keycloak realm.

Go to [Keycloak Admin Console](http://localhost:8080/auth/admin/master/console) and make sure following things are setup:

* realm called `symentis-micro-services`
* within this realm you will need to clients
 * `symentis-frontend` with access type `public`, it will be used to authenticate using username and password
  * `symentis-micro-services-api` with access type `bearer-only`, it will be used to authenticate calls between microservices
* new role called `USER`
* new user called `symentis-micro` with password `symentis-micro` with added role `USER`

You can also use a different option and import pre configured export file,
you can find it in `deploy/keycloak-export.json`. In order to do it, start
Keycloak with docker and login to Admin Console. Once you are in, create new
realm called, `symentis-micro-services`. Go to newly create realm and pick
Manage-Import menu option (from lefthand side menu). Select
`deploy/keycloak-export.json`, then "Import from realm" set to
`symentis-micro-services` and `If a resource exists` select `Overwrite`, submit
import and enjoy working keycloak. You can test if it works and generate access
token.

## Generate access token

In order to generate access token to be used in calls to microservices, use keycloak REST API:

```
export ACCESS_TOKEN=`curl -s -X POST --data "grant_type=password&client_id=symentis-frontend&username=symentis-micro&password=symentis-micro" http://localhost:8080/auth/realms/symentis-micro-services/protocol/openid-connect/token | jq ".access_token" | sed 's/"//g'`
```

WARNING: make sure `curl` and `jq` packages are installed

Once you get access token you can all services like this:

```
curl -H "Authorization: Bearer $ACCESS_TOKEN" http://localhost:9090/api/point-of-sale
```
## Scripts

This project contains a set of helper scripts which ease work with
whole infrastructure of docker containers. Please go to `scripts`
directory and refer to individual scripts documentation, in comments
section.

# TODO

* check how to make keycloak work with roles
* how to mock keycloak in integration tests
* configuration of keycloak in docker compose
* write end to end test suite,
* make sure there is no [double filter registration](https://keycloak.gitbooks.io/securing-client-applications-guide/content/topics/oidc/java/spring-security-adapter.html)
