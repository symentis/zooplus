package pl.symentis.micro.customers.endpoints;

import java.time.LocalDate;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import com.google.common.collect.ImmutableList;

import pl.symentis.micro.customers.models.Customer;

@Path("/customers")
@Produces(MediaType.APPLICATION_JSON)
@Component
public class Customers {

	@GET
	public Response listCustomers(){

		ImmutableList<Customer> customers = ImmutableList.of(new Customer("Jarosław","Pałka",LocalDate.of(1975, 10, 30),"M"));

		return Response.ok(customers).build();
	}

}
