package pl.symentis.micro.customers.endpoints;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@ApplicationPath("/api")
@Component
public class CustomersResourceConfig extends ResourceConfig{

	public CustomersResourceConfig() {
		register(Customers.class);
	}
	
}
