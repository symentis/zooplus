package pl.symentis.micro.customers.models;

import java.time.LocalDate;

public class Customer {

	private final String firstname;
	private final String lastname;
	private final LocalDate birthdate;
	private final String sex;

	public Customer(String firstname, String lastname, LocalDate birthdate, String sex) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.birthdate = birthdate;
		this.sex = sex;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public String getSex() {
		return sex;
	}

}
