package pl.symentis.micro.customers;

import static com.jayway.restassured.RestAssured.given;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.is;

import org.apache.http.HttpStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration")
public class CustomersApplicationTestsIT {
	
	@LocalServerPort
	private int localServerPort;

	@Test
	public void list_all_customers() {
		
		RestAssured.port = localServerPort;
		
		given()
			.basePath("/api/customers")
		.when()
			.get()
		.then()
			.statusCode(HttpStatus.SC_OK)
			.contentType(ContentType.JSON)
			.body("firstname", is(asList("Jarosław")))
			.body("lastname", is(asList("Pałka")))
			.body("birthdate", is(asList("1975-10-30")))
			.body("sex",is(asList("M")));
	}

}
