package pl.symentis.micro.customers;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.RestAssured;

import pl.symentis.micro.frontend.FrontendApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=FrontendApplication.class,webEnvironment=WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration")
public class FrontendApplicationTestsIT {
	
	@LocalServerPort
	private int localServerPort;

	@Test
	@Ignore("problem with injection local server port")
	public void home_page() {
		RestAssured.port = localServerPort;
	}

}
