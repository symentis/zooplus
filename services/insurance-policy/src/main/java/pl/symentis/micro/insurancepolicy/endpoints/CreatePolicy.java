package pl.symentis.micro.insurancepolicy.endpoints;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CreatePolicy {

	private final Customer customer;
	private final Product product;

	@JsonCreator
	public CreatePolicy(
			@JsonProperty("product") Product product, 
			@JsonProperty("customer") Customer customer) {
		this.product = product;
		this.customer = customer;
	}

	public Customer getCustomer() {
		return customer;
	}

	public Product getProduct() {
		return product;
	}

}
