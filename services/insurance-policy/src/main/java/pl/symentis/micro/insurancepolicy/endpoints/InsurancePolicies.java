package pl.symentis.micro.insurancepolicy.endpoints;

import java.net.URI;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

@Path("/policies")
@Component
public class InsurancePolicies {

	@GET
	public Response listPolicies(){		
		return Response.ok().build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createPolicy(CreatePolicy insurancePolicy){
		return Response.created(URI.create("http://insurance-policy/api/policies/1")).build();
	}
	
}
