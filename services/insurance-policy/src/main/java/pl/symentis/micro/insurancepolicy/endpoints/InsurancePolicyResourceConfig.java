package pl.symentis.micro.insurancepolicy.endpoints;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@ApplicationPath("/api")
@Component
public class InsurancePolicyResourceConfig extends ResourceConfig{

	public InsurancePolicyResourceConfig() {
		register(InsurancePolicies.class);
	}

}
