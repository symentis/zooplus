package pl.symentis.micro.insurancepolicy.endpoints;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Product {

	private final String name;

	private final LocalDateTime availableUntil;

	@JsonCreator
	public Product(
			@JsonProperty("name")String name, 
			@JsonProperty("availableUntil") LocalDateTime availableUntil) {
		super();
		this.name = name;
		this.availableUntil = availableUntil;
	}

	public String getName() {
		return name;
	}

	public LocalDateTime getAvailableUntil() {
		return availableUntil;
	}

}
