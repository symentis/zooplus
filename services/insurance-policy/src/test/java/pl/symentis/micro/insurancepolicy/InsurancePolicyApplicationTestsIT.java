package pl.symentis.micro.insurancepolicy;

import static com.jayway.restassured.RestAssured.given;

import javax.ws.rs.core.MediaType;

import org.apache.http.HttpStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.RestAssured;

import pl.symentis.micro.insurancepolicy.endpoints.CreatePolicy;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration")
public class InsurancePolicyApplicationTestsIT {
	
	@LocalServerPort
	private int localServerPort;
	
	@Test
	public void contextLoads() {
		
		RestAssured.port=localServerPort;
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		
		given()
			.contentType(MediaType.APPLICATION_JSON)
			.body(new CreatePolicy(null,null))
		.when()
			.post("/api/policies")
		.then()
			.statusCode(HttpStatus.SC_CREATED)
			.log();
			
	}

}
