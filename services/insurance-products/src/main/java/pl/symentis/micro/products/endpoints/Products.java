package pl.symentis.micro.products.endpoints;

import java.time.LocalDateTime;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import com.google.common.collect.ImmutableList;

import pl.symentis.micro.products.models.Product;

@Path("/products")
@Produces(MediaType.APPLICATION_JSON)
@Component
public class Products {

	@GET
	public Response list(){
		ImmutableList<Product> products = ImmutableList.of(new Product("Polisa AC", LocalDateTime.of(2017, 12, 30, 23, 59)));
		return Response.ok(products).build();
	}
	
}
