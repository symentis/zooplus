package pl.symentis.micro.products.endpoints;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@ApplicationPath("/api")
@Component
public class ProductsResourceConfig extends ResourceConfig {

	public ProductsResourceConfig() {
		register(Products.class);
	}

}
