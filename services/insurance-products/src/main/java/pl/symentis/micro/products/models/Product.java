package pl.symentis.micro.products.models;

import java.time.LocalDateTime;

public class Product {

	private final String name;

	private final LocalDateTime availableUntil;

	public Product(String name, LocalDateTime availableUntil) {
		super();
		this.name = name;
		this.availableUntil = availableUntil;
	}

	public String getName() {
		return name;
	}

	public LocalDateTime getAvailableUntil() {
		return availableUntil;
	}

}
