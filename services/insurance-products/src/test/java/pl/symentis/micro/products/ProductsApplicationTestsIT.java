package pl.symentis.micro.products;

import static com.jayway.restassured.RestAssured.given;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.is;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration")
public class ProductsApplicationTestsIT {
	
	@LocalServerPort
	private int localServerPort;

	@Test
	public void list_all_insurance_products() {
		RestAssured.port = localServerPort;
		given()
			.basePath("/api/products")
		.when()
			.get()
		.then()
			.statusCode(200)
			.contentType(ContentType.JSON)
			.body("name",is(asList("Polisa AC")))
			.body("availableUntil",is(asList("2017-12-30T23:59:00")));
	}

}
