package pl.symentis.micro.pointofsale;

import java.io.IOException;

import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.client.RestTemplate;

/**
 * 
 * Custom request interceptor which rewrites bearer token from current server
 * HTTP request to a client HTTP request. This is a nicer approach then
 * originial {@link KeycloakRestTemplate} as this doesn't create new
 * {@link RestTemplate} instance per each request.
 * 
 * @author jaroslaw.palka@symentis.pl
 *
 */
final class KeycloakAccessTokenClientHttpRequestInterceptor implements ClientHttpRequestInterceptor {

	public static final String AUTHORIZATION_HEADER = "Authorization";

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {
		postProcessHttpRequest(request);
		return execution.execute(request, body);
	}

	void postProcessHttpRequest(HttpRequest request) {
		KeycloakSecurityContext context = this.getKeycloakSecurityContext();
		request.getHeaders().add(AUTHORIZATION_HEADER, "Bearer " + context.getTokenString());
	}

	/**
	 * Returns the {@link KeycloakSecurityContext} from the Spring
	 * {@link SecurityContextHolder}'s {@link Authentication}.
	 *
	 * @return the current <code>KeycloakSecurityContext</code>
	 */
	KeycloakSecurityContext getKeycloakSecurityContext() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		KeycloakAuthenticationToken token;
		KeycloakSecurityContext context;

		if (authentication == null) {
			throw new IllegalStateException(
					"Cannot set authorization header because there is no authenticated principal");
		}

		if (!KeycloakAuthenticationToken.class.isAssignableFrom(authentication.getClass())) {
			throw new IllegalStateException(String.format(
					"Cannot set authorization header because Authentication is of type %s but %s is required",
					authentication.getClass(), KeycloakAuthenticationToken.class));
		}

		token = (KeycloakAuthenticationToken) authentication;
		context = token.getAccount().getKeycloakSecurityContext();

		return context;
	}

}