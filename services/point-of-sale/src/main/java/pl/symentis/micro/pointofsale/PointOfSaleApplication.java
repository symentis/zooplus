package pl.symentis.micro.pointofsale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
public class PointOfSaleApplication {

	public static void main(String[] args) {
		SpringApplication.run(PointOfSaleApplication.class, args);
	}

	// @LoadBalanced
	// @Bean
	// @Profile("!integration")
	// public RestTemplate securedRestTemplate(){
	// return new RestTemplateBuilder()
	// .additionalInterceptors(new
	// KeycloakAccessTokenClientHttpRequestInterceptor())
	// .build();
	// }

	// TODO is this the smartest way to do it? don't think so
	@Bean
	// @Profile("integration")
	@LoadBalanced
	public RestTemplate restTemplate() {
		return new RestTemplateBuilder().build();
	}
}
