package pl.symentis.micro.pointofsale.endpoints;

import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.common.collect.ImmutableList;

import pl.symentis.micro.pointofsale.models.CreatePolicy;
import pl.symentis.micro.pointofsale.models.Customer;
import pl.symentis.micro.pointofsale.models.InsurancePolicySaleRequest;
import pl.symentis.micro.pointofsale.models.Product;

@Path("/point-of-sale")
@Component
public class PointOfSale {

	private final RestTemplate restTemplate;

	@Autowired
	public PointOfSale(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response listSales(InsurancePolicySaleRequest saleRequest) {
		
		CreatePolicy policy = newPolicy(products(), customers());

		ResponseEntity<Object> responseEntity = restTemplate.postForEntity("http://insurance-policy/api/policies",policy, null);

		if(HttpStatus.CREATED.equals(responseEntity.getStatusCode())){
			return Response.created(responseEntity.getHeaders().getLocation()).build();
		}
		
		return Response.seeOther(responseEntity.getHeaders().getLocation()).build();
	}
	
	private CreatePolicy newPolicy(Optional<Product> product, Optional<Customer> customer){
		return new CreatePolicy(product.get(), customer.get());
	}

	private Optional<Customer> customers() {
		ImmutableList<Customer> products = ImmutableList
				.copyOf(restTemplate.getForObject("http://customers/api/customers", Customer[].class));
		return products.stream().findFirst();
	}

	private Optional<Product> products() {
		ImmutableList<Product> products = ImmutableList
				.copyOf(restTemplate.getForObject("http://insurance-products/api/products", Product[].class));
		return products.stream().findFirst();
	}

}
