package pl.symentis.micro.pointofsale.endpoints;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@ApplicationPath("/api")
@Component
public class PointOfSaleResourceConfig extends ResourceConfig {

	public PointOfSaleResourceConfig() {
		register(PointOfSale.class);
	}

}
