package pl.symentis.micro.pointofsale.models;

public class CreatePolicy {

	private final Customer customer;
	private final Product product;

	public CreatePolicy(Product product, Customer customer) {
		this.product = product;
		this.customer = customer;
	}

	public Customer getCustomer() {
		return customer;
	}

	public Product getProduct() {
		return product;
	}

}
