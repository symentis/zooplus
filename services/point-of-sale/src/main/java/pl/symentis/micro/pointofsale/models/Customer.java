package pl.symentis.micro.pointofsale.models;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Customer {

	private final String firstname;
	private final String lastname;
	private final LocalDate birthdate;
	private final String sex;

	@JsonCreator
	public Customer(
			@JsonProperty("firstname") String firstname, 
			@JsonProperty("lastname") String lastname, 
			@JsonProperty("birthdate") LocalDate birthdate, 
			@JsonProperty("sex") String sex) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.birthdate = birthdate;
		this.sex = sex;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public String getSex() {
		return sex;
	}
	
	

}
