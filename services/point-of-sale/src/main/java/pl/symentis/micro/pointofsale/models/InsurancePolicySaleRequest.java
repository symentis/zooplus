package pl.symentis.micro.pointofsale.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class InsurancePolicySaleRequest {

	private final String productName;
	private final String firstname;
	private final String lastname;

	@JsonCreator
	public InsurancePolicySaleRequest(
			@JsonProperty("productName") String productName, 
			@JsonProperty("firstname") String firstname, 
			@JsonProperty("lastname") String lastname) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.productName = productName;
	}

	public String getProductName() {
		return productName;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}
	
}
