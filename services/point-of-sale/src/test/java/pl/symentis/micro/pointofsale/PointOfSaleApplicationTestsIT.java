package pl.symentis.micro.pointofsale;

import static com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder.okForJson;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.jayway.restassured.RestAssured.given;
import static java.util.Arrays.asList;
import static org.apache.http.HttpStatus.SC_CREATED;

import javax.ws.rs.core.MediaType;

import org.apache.http.HttpStatus;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.common.Slf4jNotifier;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.jayway.restassured.RestAssured;

import pl.symentis.micro.pointofsale.models.Customer;
import pl.symentis.micro.pointofsale.models.InsurancePolicySaleRequest;
import pl.symentis.micro.pointofsale.models.Product;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration")
public class PointOfSaleApplicationTestsIT {
	
	@Rule
	public WireMockRule services = new WireMockRule(new WireMockConfiguration().port(8090).notifier(new Slf4jNotifier(true)));
	
	@LocalServerPort
	private int localServerPort;

	@Test
	public void contextLoads() {
				
		services
			.stubFor(
					get(urlPathEqualTo("/api/products"))
					.willReturn(okForJson(asList(new Product("Polisa AC", null)))));
		
		services
			.stubFor(
					get(urlPathEqualTo("/api/customers"))
					.willReturn(okForJson(asList(new Customer("Jan", "Kowalski", null, "M")))));
		

		services
			.stubFor(
				post(urlPathEqualTo("/api/policies"))
				.willReturn(WireMock.aResponse().withStatus(SC_CREATED)));
		
		RestAssured.port = localServerPort;
				
		given()
			.basePath("/api/point-of-sale")
			.content(new InsurancePolicySaleRequest("Polisa AC","Jan","Kowalski"))
			.contentType(MediaType.APPLICATION_JSON)
		.when()
			.post()
		.then()
			.statusCode(HttpStatus.SC_CREATED)
			.log();
		
	}

}
